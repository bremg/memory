(function(win, doc, undefined) {
    if (!(doc.querySelector && doc.addEventListener)) return;
    'use strict';

    win.Memory = {};

    (function(ns){

        ns.selectors = {
            dataActiveCard: 'data-active-class',
            dataMatchedCard: 'data-matched-class',
            cssActiveCard: '',
            cssMatchedCard: ''
        };

        ns.loop = function _loop(items, func){
            var i = items.length;
            for(;i--;) func.apply(items, [items[i], i]);
        };

        ns.totalCards = function totalCards() {
            return document.querySelector('.js-deck').childElementCount;
        };

        ns.initialNumberOfCards = ns.totalCards();

        ns.cardsLeft = function cardsLeft() {
            if(!ns.selectors.cssMatchedCard.length > 0) return ns.totalCards();

            var currentAmountOfCards = document.querySelectorAll('.' + ns.selectors.cssMatchedCard).length;
            return ns.initialNumberOfCards - currentAmountOfCards;
        };

        ns.setCardsLeft = function setCardsLeft() {
            var counter = document.querySelector('.js-cards-left > p');
            counter.innerHTML = "Cards left: " + ns.cardsLeft();
        };

        ns.match = function _match(previousCard, currentCard){

            ns.loop([previousCard, currentCard], function setClasses(el) {
                el.classList.remove(ns.selectors.cssActiveCard);
                el.classList.add(ns.selectors.cssMatchedCard);
            });

            ns.setCardsLeft();

            if(ns.cardsLeft() === 0){
                setTimeout(function(){
                    window.alert("Gefeliciteerd je hebt gewonnen!");
                }, 500);
            }
        };

        ns.noMatch = function _noMatch(previousCard, currentCard){
            setTimeout(function(){
                previousCard.classList.toggle(ns.selectors.cssActiveCard);
                currentCard.classList.toggle(ns.selectors.cssActiveCard);
            }, 500);
        };

        ns.makeMove = function _makeMove(currentCard, callback){
            var cardName;
            var previousCard;

            if(ns.selectors.cssActiveCard.length === 0 || ns.selectors.cssMatchedCard.length === 0 ){ //set selector
                ns.selectors.cssActiveCard = currentCard.getAttribute(ns.selectors.dataActiveCard);
                ns.selectors.cssMatchedCard = currentCard.getAttribute(ns.selectors.dataMatchedCard);
            }

            previousCard = document.querySelector('.' + ns.selectors.cssActiveCard);

            if(previousCard === currentCard) return callback(); //double click

            currentCard.classList.toggle(ns.selectors.cssActiveCard);
            cardName = currentCard.getAttribute('data-card-name');

            if(!previousCard) return callback(); //No second card to match with

            if(previousCard.getAttribute('data-card-name') === cardName){  //match
                ns.match(previousCard, currentCard);
            } else {
                ns.noMatch(previousCard, currentCard);
            }

            setTimeout(function(){
                return callback();
            }, 500);
        };

    })(Memory);


    (function _clickCard(deck){
        if(!deck) return;

        var parent;

        deck.addEventListener('click', function _click(e) {
            if( deck.getAttribute('stillBusy') === 'active' ) return;

            deck.setAttribute('stillBusy', 'active');
            parent = e.target.parentElement;

            if (!parent.classList.contains('js-card') || parent.classList.contains(Memory.selectors.cssMatchedCard)){
                return deck.removeAttribute('stillBusy');
            }

            Memory.makeMove(parent, function(){
                deck.removeAttribute('stillBusy');
            });
        });

    })(document.querySelector('.js-deck'));

    (function(selector){
        if(!selector) return;
        selector.innerHTML += Memory.totalCards();
    })(document.querySelector('.js-cards-total > p'));

    (function(){
        Memory.setCardsLeft();
    }());

})(window, document);

