<!doctype html>
<html lang="nl">
    <head>
        <meta charset="utf-8">
        <meta http-equiv="X-UA-Compatible" content="IE=edge">
        <meta name="viewport" content="width=device-width, initial-scale=1">

        <title>{{ $title }}</title>

        @if(env('INLINE_CSS', false))
            <style>{!! file_get_contents(public_path('assets/css/style.css')) !!}</style>
        @else
            <link rel="stylesheet" href="{{ elixir('assets/css/style.css') }}">
        @endif
    </head>

    <body>

    <div class="scoreboard">
        <h1>{{ $title }}</h1>

        <div class="game-monitor">
            <div class="game-monitor__cards-total js-cards-total">
                <p>Total number of cards: </p>
            </div>
            <div class="game-monitor__cards-left js-cards-left">
                <p>Cards left: </p>
            </div>
        </div>
    </div>

    <div class="memory-container">


        <ul class="deck js-deck">
            @foreach($tiles as $tile)
                @include('includes.tile')
            @endforeach
        </ul>
    </div>

    @include('includes.footer_js')
    </body>
</html>
