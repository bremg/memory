<li class="card js-card" data-card-name="{{$tile}}" data-active-class="card--active" data-matched-class="card--matched">
    <img class= 'card__image' src="{{asset('assets/images/tiles/' .$tile.'.png')}}" alt="{{$tile}}">
</li>
