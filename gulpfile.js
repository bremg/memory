let elixir = require('laravel-elixir');
let gulp = require('gulp');
let imagemin = require('gulp-imagemin');
let sourcemaps = require('gulp-sourcemaps');
let autoprefixer = require('gulp-autoprefixer');
let publicPath = 'public';
let staticCssPath = publicPath + '/assets/css/style.css';

elixir.config.sourcemaps = true; // to make source maps are generated even with --production flag
elixir.config.publicPath = publicPath;

elixir(function _elixir(mix) {
    mix.sass('style.scss', staticCssPath);
    mix.task('autoprefixer');

    mix.scripts('footer_js.js', publicPath + '/assets/js/footer_js.js');

    mix.version([staticCssPath]);
});

gulp.task('autoprefixer', function _autoprefixer() {
    let browsers = ['last 2 versions', 'IE 10'];
    let opts = {
        browsers: browsers,
        cascade: false
    };

    gulp.src(staticCssPath)
        .pipe(sourcemaps.init())
        .pipe(autoprefixer(opts))
        .pipe(sourcemaps.write('.'));
});

gulp.task('img', function _img() {
    gulp.src('resources/assets/images/**')
        .pipe(imagemin())
        .pipe(gulp.dest(publicPath + '/assets/images'))
});
