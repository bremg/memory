# Opdracht

Maak een werkend memory spel met de volgende requirements

## Requirements

 - Design is niet belangrijk
 - Denk om best practices, performance en onderhoudbaarheid
 - Houd html, css en js gescheiden (content, uiterlijk, gedrag)
 - IE10 support
 - Animeer het omdraaien
 - Je mag pas een volgende tegel omdraaien als je klaar bent met het omdraaien van de vorige (animatie moet klaar zijn)
 - Een gevonden paar moet uit het speeldveld gehaald worden.
 - Toon een melding wanneer alle paren gevonden zijn (window.alert is prima)

## Extra

Onderstaande is niet verplicht maar misschien leuk als je extra uitdaging wilt en/of lekker bezig bent

- css grid voor de layout
- 1 element voor de tegel
- Werkend met toetsenbord / dus geen muis nodig

## Wat krijg je aangeleverd

Een start project

 - In git op bitbucket
 - Laravel met blade als template taal
 - De benodigde afbeeldingen
 - Werkende build tool (gulp) ingesteld voor gebruik met sass
 
## Hoe lever je het in?
Fork het project op bitbucket. En als je klaar bent geef je mij de url + lees rechten tot je repo.

# Project
1. `composer install`
2. `yarn install`

Vervolgens `gulp watch` om je css/js te watchen.

Voor dat je gaat pushen eerst een `gulp --production` uitvoeren

Indien je nieuwe afbeeldingen plaatst of afbeelingen wijzigt: niet vergeten om `gulp img` te runnen. 
