<?php

namespace App\Http\Controllers;

use Illuminate\Foundation\Bus\DispatchesJobs;
use Illuminate\Routing\Controller as BaseController;
use Illuminate\Foundation\Validation\ValidatesRequests;
use Illuminate\Foundation\Auth\Access\AuthorizesRequests;

class Controller extends BaseController
{
    use AuthorizesRequests, DispatchesJobs, ValidatesRequests;

    public function index()
    {
        $title = 'Memory';
        $tiles = array_merge($this->getTiles(), $this->getTiles());

        shuffle($tiles);

        return view('main', [
            'title' => $title,
            'tiles' => $tiles,
        ]);
    }

    protected function getTiles()
    {
        return [
            'amethyst',
            'anchor',
            'boar',
            'boat',
            'butterfly',
            'chest',
            'clover4',
            'deer',
            'dragonfly',
            'eagle',
            'fish',
            'flower',
            'fox',
            'goat',
            'hare',
            'lantern',
            'lemming',
            'lotus',
            'pickaxe',
            'raft',
            'rose',
            'saw',
            'sheep',
            'shell',
            'sickle',
            'straw',
            'swan',
            'turtle',
            'white-flower',
            'white-flower2',
        ];
    }
}
